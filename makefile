CC=g++ -fPIC -std=c++0x

SRC=$(wildcard src/*.cpp) $(wildcard src/lib/*.c*)
OBJ=$(SRC:.cpp=.o)

CFLAGS=-Wno-write-strings 
CFLAGSAPP=-l 3dnome -I"./src"  -L"./" -Wl,-rpath="./"

.PHONY : all

all: 3dnome 3dnome-app

3dnome:
		$(CC) $(CFLAGS) -shared $(SRC) -o lib3dnome.so

3dnome-app:
		$(CC) $(CFLAGS) src/tools/main.cpp -o 3dnome $(CFLAGSAPP)

clean:
		rm -fr *.o
		rm lib3dnome.so
		rm 3dnome

